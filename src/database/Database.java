package database;

import assistant.Assistant;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Database implements Runnable {

    public static final int PORT = 3000;

    private final ServerSocket databaseSocket = new ServerSocket(PORT);

    private final List<Assistant> assistants = new ArrayList<>();

    private Database() throws IOException {}

    private boolean running = true;

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean running) {
        this.running = running;
    }

    void addAssistant(Assistant assistant) {
        if (assistants.contains(assistant))
            assistants.get(assistants.indexOf(assistant)).addScore(assistant.getScore());
        else
            assistants.add(assistant);
    }

    public List<Assistant> getAssistants() {
        return Collections.unmodifiableList(assistants);
    }

    @Override
    public void run() {
        try {
            while (running) {
                final Socket socket = databaseSocket.accept();

                final BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

                final PrintWriter output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);

                new Thread(new ClientHandler(this, socket, input, output)).start();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            new Thread(new Database()).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
