package database;

import assistant.Assistant;
import com.google.gson.Gson;
import servlet.ServletGetAll;
import servlet.ServletSave;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

class ClientHandler implements Runnable {

    private final Database database;

    private final Socket client;

    private final BufferedReader input;

    private final PrintWriter output;

    private final Gson gson = new Gson();

    ClientHandler(Database database, Socket client, BufferedReader input, PrintWriter output) {
        this.database = database;
        this.client   = client;
        this.input    = input;
        this.output   = output;
    }

    @Override
    public void run() {
        try {
            final String request = input.readLine();

            if (request.equals(ServletGetAll.GET_ALL_REQUEST))
                returnAllAssistants();
            else if (request.equals(ServletSave.SAVE_REQUEST))
                saveAssistant();
        } catch (IOException e) {
            e.printStackTrace();
        }

        closeConnection();
    }

    private void returnAllAssistants() {
        final List<Assistant> assistants = database.getAssistants();

        assistants.forEach(assistant -> output.println(gson.toJson(assistant)));
    }

    private void saveAssistant() throws IOException {
        final String assistantJSON = input.readLine();

        final Assistant assistant = gson.fromJson(assistantJSON, Assistant.class);

        database.addAssistant(assistant);
    }

    private void closeConnection() {
        try {
            client.close();
            input.close();
            output.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return String.format("ClientHandler:[database=%s, client=%s, input=%s, output=%s]",
                database, client, input, output);
    }
}
