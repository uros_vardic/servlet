package assistant;

import java.util.Objects;

public class Assistant {

    private final String name;

    private int score;

    private int numberOfEntries;

    public Assistant(String name, int score) {
        this.name = name.toUpperCase();

        if (this.name.equals("VUK"))
            this.score = 0;
        else
            this.score = score;

        numberOfEntries++;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }

    public void addScore(int score) {
        if (score < 0 || score > 10)
            throw new IllegalArgumentException(String.format("Invalid score: %d, score interval (0, 10)", score));

        this.score += score;

        numberOfEntries++;
    }

    public int calculateAverageScore() {
        return score / numberOfEntries;
    }

    @Override
    public String toString() {
        return String.format("Assistant:[name=%s, score=%d]", name, score);
    }

    @Override
    public boolean equals(Object reference) {
        if (reference instanceof Assistant) {
            final Assistant other = (Assistant) reference;

            return Objects.equals(this.name, other.name);
        }

        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name);
    }
}
