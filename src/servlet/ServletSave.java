package servlet;

import assistant.Assistant;
import com.google.gson.Gson;
import database.Database;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.Socket;

@WebServlet(name = "ServletSave", urlPatterns = {"/assistant"})
public class ServletSave extends HttpServlet {

    public static final String SAVE_REQUEST = "SAVE-ASSISTANT";

    private final Gson gson = new Gson();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final PrintWriter out = response.getWriter();

        response.setContentType("text/html");

        saveAssistant(request);

        out.println("<body><h1>Entry saved</h1></body>");

        out.close();
    }

    private void saveAssistant(HttpServletRequest request) throws IOException {
        final String name = request.getParameter("name");

        final int score = Integer.parseInt(request.getParameter("score"));

        final Assistant assistant = new Assistant(name, score);

        final Socket socket = new Socket("localhost", Database.PORT);

        final PrintWriter output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);

        output.println(SAVE_REQUEST);
        output.println(gson.toJson(assistant));
    }
}
