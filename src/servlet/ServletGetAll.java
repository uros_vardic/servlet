package servlet;

import assistant.Assistant;
import com.google.gson.Gson;
import database.Database;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.Socket;

@WebServlet(name = "ServletGetAll", urlPatterns = {"/assistant/get_all"})
public class ServletGetAll extends HttpServlet {

    public static final String GET_ALL_REQUEST = "GET-ALL-ASSISTANTS";

    private final Gson gson = new Gson();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final Socket socket = new Socket("localhost", Database.PORT);

        final BufferedReader input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        final PrintWriter output = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);

        output.println(GET_ALL_REQUEST);

        final PrintWriter out = response.getWriter();

        response.setContentType("text/html");

        out.println("<body>");
        out.println("<h1>Assistants:</h1>");

        String assistantJSON = input.readLine();

        while (assistantJSON != null) {
            final Assistant assistant = gson.fromJson(assistantJSON, Assistant.class);

            final String assistantInfo = assistant.getName() + " " + assistant.calculateAverageScore();

            out.println("<h3>" + assistantInfo + "</h3>");

            assistantJSON = input.readLine();
        }

        out.println("</body>");
        out.close();
    }
}
